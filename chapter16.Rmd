---
title: "Chapter 16 - Identification of the model and estimation of parameters"
author: "Diletta Olliaro"
date: "4/7/2020"
output: pdf_document
---

# Lab 4 - Identification of the Model and Estimation of Parameters

\textbf{Fitting of an AR(p) model}

\textbf{\textit{Simulation of an AR(2) model}}

If we want to make sure that our data really comes from an AR model the only think we can do it's simulating it.

```{r}
# we set the random seed
set.seed(1)
# we decide how many observations we want
n<-300
# we set the true values of the parameters for the model we are simulating from
phi<-c(0.6,-0.3)
# then we simulate using the arima.sim function
x<-arima.sim(n=n,model=list(ar=phi))
# now we have our data 'x' which is a time series with 300 observations
```

\textbf{\textit{Fit via regresson model method (ordinary least squares)}}

Now we are going to pretend that we do not know what the $\phi$s are and we are going to see if we can recover those parameters. Of course, in this case we know the value of $p$, if we did not know we had to draw the PACF function and find the cut-off.\

We use the regression method by using the ordinary least squares

```{r}
# this function is actually the ordinary least square equation for the regression
# we set aic (Aikake infotmation) to FALSE because we do not want to use a criterium
# we only want to fit the maximum possible order p = 2
fit1<-ar.ols(x, aic = FALSE, order.max = 2)
fit1
```

Output is telling us which is the function that we called,it's estimating the coefficients ($\phi_1$ and $\phi_2$), if we look at them we notice that we do not get exactly tha values we choose for $\phi_1$ and $\phi_2$ but they are not too far, if we wanted to check if these values were significantly different from the true values we should run a formal test. Moreover it's also telling us that the intercept of the regression model(the $\alpha$) it's 0.009882 with a standard error of 0.05575 so it's telling us that it's practically zero and the standard error is not too big consequently we can state $\alpha=0$ and this should not be surprising because that is what we simulate from. Finally we get this final parameter which is the $\sigma^2$ for the white noise, we know the true value is 1 because with the function \texttt{arima.sim} if we do not introduce any other of the optional parameters it's assuming that the white noise is Gaussian with mean 0 and variance 1, consequently we notice the value we get is not too far from the one we simulated from.

\textbf{\textit{Fit via Yule-Walker equations}}


We know there is another way to fit the model which is Yule-Walker equations, we know we got a matrix form for these equations and we can solve the system of equations by inverting the matrix of estimated autocorrelations and this is done with the function \texttt{ar.yw}. Again we do not want to use any criteria (so \texttt{aic=FALSE}), we just want to fit our model with $p=2$.

```{r}
fit2<- ar.yw(x,aic=FALSE,order.max = 2)
fit2
```

We can see the values we get are not exactly the same of the ones we got with the ordinary least squares but they are very very close. So the two methods are giving very similar values for the parameters and also for the estimated $\sigma^2$. What is the difference? Ordinary least squares is fittinga a regression model, and the regression model could have an intercept, whereas the Yule-Walker equations method only works for models with mean 0 (as a matter of fact before we can apply this method we have to remove the mean) consequently it is not giving an estimate for the $\alpha$ because it assumes the $\alpha$ does not exist or that is equal to 0.

\textbf{\textit{Real data example}}


What happens if we take the \texttt{LakeHuron} data and we fit an autoregressive model of order 3.

```{r}
fit1<-ar.ols(LakeHuron, aic = FALSE, order.max = 3)
fit1
fit2<- ar.yw(LakeHuron,aic=FALSE,order.max = 3)
fit2
```
The values we get are not exactly the same but they are roughly similar. If we look at the output of \texttt{ar.ols} we see an intercept but it is small, if we wanted to know if it is small enough to be considered zero we should run a test. Then we have the two estimates for $\sigma^2$ which is certainly not 1 but it should be around 0.5 (this would be reasonable). Is this a good fit? We do not know unless we run a test and with this we check the residuals and maybe compare this model to others.

\textbf{Fitting of an MA(q) model}


We know that when we have an moving average component we cannot do estimation directly.

\textbf{\textit{Simulation of an MA(1) model}}

We simulate our data:

```{r}
set.seed(5)
x <-arima.sim(list(order=c(0,0,1), ma=0.9), n=500)
acf.x<- acf(x,plot=TRUE)$acf
```



\textbf{\textit{Fit via method of moments}}

We can use the method of moments. First of all we need to calculate the empirical autocorrelations. 

```{r}
# The method of moments consists of saying autocorrelation at lag 1 (r1) is equal 
# to the expression for the autocorrelation, 
r1<-acf.x[2]
r1
# this gives us an equation, that depends on this autocorrelation, which basically 
# is a polynomial equations with the following coefficients
polyroot(c(r1,-1,r1))
```

solving this equation sould give as a result the $\theta_1$ parameter of our moving average process but because the values we obtain are bigger than 0.5 the roots of the equation are complex and so the method of moments actually fails. Hence, this is a practical example of the fact that even when our data actually come from a MA model the method of moments could fail to show that because the empirical autocorrelation should be close to the real values it's not exactly the same.

\textbf{Fitting of an ARMA(p,q) model}

In order to fit a Moving Average we need to do something more elaborated which is equivalent to what we do for general ARMA models. So even if we only have a MA component the procedure is the same of when we have them both.

\textbf{\textit{Simulation of an ARMA(1,2) model}}


```{r}
set.seed(1)
n<-300
# coefficients for the AR part
phi<-c(0.6)
# coefficients for the MA part
theta<-c(0.7,0.2)
x<-arima.sim(n=n,model=list(ar=phi,ma=theta))
```



\textbf{\textit{Implementation of the Hannan-Rissanen algorithm}}


First method we saw to fit ARMA models, it consists in iteratively fitting regression models. There is no an R internal function that implements the algorithm, so we do it below:

```{r}
# (hannan.rissanen function adapted from the itsmr package)
# as input it takes the data, the p and the q from the model that we are fitting
hannan.rissanen<- function(x,p,q) {
  # if q is less than one it means we do not have a MA component so 
  # we do not need to use this method.
  if (q < 1)
    stop("q < 1")
  # we save how many data points we have
  n <- length(x)
  # we subtract the mean, we redefine the data as data centered at zero 
  # because the algorithm only work if we actually do not have an alpha
  x <- x - mean(x)
  # m is called p* in the slides, this value (from which we start iterating)
  # is a rule of thumb and then we will get close to the value we actually need
  m <- 20 + p + q 
  k <- max(p,q)   
  # Fit an AR(m) (the phi obtained is phi* in the slides), 
  # remember that this value (m) needs to be greater of both p and q
  phi <- ar.yw(x,aic=FALSE,order.max=m,demean=FALSE)$ar
  # This function extracts the residuals of the AR(m) process, what it is doing 
  # is to take each of value of our data and subtract the estimate 
  # that we would have from the fitted model
  F1<-function(t) x[t] - sum(phi * x[(t-1):(t-m)])
  #z is eta hat in the slides
  z<-sapply((m+1):n,F1)
  z<-c(numeric(m),z)
  # we are going to fit a regression model on the AR component of the data 
  # and the estimated residuals (eta) so we are going 
  # to check the design matrix for the regression model
  F2<-function(i) z[(m+k+i-1):(m+k+i-q)]
  Z<-sapply(1:(n-m-k),F2)
  Z<-matrix(Z,nrow=n-m-k,ncol=q,byrow=TRUE)
  if (p > 0) {
    F3<-function(i) x[(m+k+i-1):(m+k+i-p)]
    X<-sapply(1:(n-m-k),F3)
    X<-matrix(X,nrow=n-m-k,ncol=p,byrow=TRUE)
    Z<-cbind(X,Z)
  } 
  # Find the least squares regression coefficients, this is done to fit the regression 
  # (this solves the least squares and finds the estimated coefficients)
  G<-qr.solve(qr(t(Z) %*% Z))
  b<-G %*% t(Z) %*% x[(m+1+k):n]
  # and then we extract again the resiudals from this model
  xhat<-Z %*% b
  # e is te estimate of epsilon
  e<-x[(m+1+k):n] - xhat 
  # estimate of sigma^2
  sigma2<-sum(e^2) / (n-m-k)
  # standard errors of the estimates, these are computed 
  # to check if we reached convergence
  se<-sqrt(sigma2*diag(G)) 
  # from now on we check if we reached convergence and if we did not we repeat
  # and repeat until we reached it.
  # AR coefficients
  if (p == 0) {
    phi<-0
    se.phi<-0
  } else {
    phi<-b[1:p]
    se.phi<-se[1:p]
  }
  # MA coefficients
  theta<-b[p+(1:q)]
  se.theta<-se[p+(1:q)]
  a<-list(phi=phi,
          theta=theta,
          se.phi=se.phi,
          se.theta=se.theta)
  return(a)
}
```


\textbf{\textit{Fit via the Hannan-Rissanen algorithm}}
```{r}
hannan.rissanen(x,1,1)

```

Since we are using data coming from an ARMA(1,2) calling the function in this way (using $p=1$ and $q=1$) we are not going to get the best fit we can since we are fitting the wrong model. As a matter of fact for the $\phi$ we get a value that is not too far from the real one, whereas for the $\theta$ (since we should have two) we get something that actually is in between of the two real values.
So, this output is the answer to the following question "what is the best ARMA(1,1) that you can fit to this data?", is this output good enough? we do not know until we analyse the residuals but this is the best we could do if we got the order of the model wrong.

\textbf{\textit{Fit via Maximum Likelihood approach}}


To fit ARMA model we could also use the Maximum Likelihood approach, in order to do so we use the \texttt{arima} function.

```{r}
# we say: I give you the data x, and the p,d,q values 
# (so we are going to fit the same, wrong, ARMA(1,1) model)
fit<-arima(x,order=c(1,0,1))
fit
```

This time we notice, we also get standard errors for our estimates that are used to get either confidence intervals for the estimated parameters or to do tests on the values of the estimated parameters.
Then we have one value for the autoregressive component and one for the moving avarage component (values are not too different from the ones we got with approach above), moreover, since we did not specified otherwise in the function, we are also getting an estimate for the $\alpha$, notice that this value is not zero (even if we simulated from a zero mean process) because the model we are fitting is wrong, meaning that when I use an ARMA(1,1) for this data (that actually comes from an ARMA(1,2)) the model needs an intercept (an $\alpha$) in order to compensatefor the missing order in the moving average component.
Unless we set otherwise, the default setup of the \texttt{arima} function is to use a Gaussian distribution as distribution for the white noise (i.e. our noise is normally distributed).

Finally we also get the aic information, if we try to fit the correct model we see that the Aikake information is slightly better and that the estimates for the parameters are closer to the real ones.

```{r}
fit.corr <- arima(x,order=c(1,0,2))
fit.corr
```


\textbf{\textit{Fit via Maximum Likelihood approach using fpp package}}


```{r message=FALSE}
# (useful for forecasting)
# this package is meant for learning how to do time series analysis
# with a particular focus on forecasting
library(fpp)
```
```{r}
# it has an Arima function (notice the capital letter), to fit Arima models
fit1 <- Arima(x, order=c(1,0,1))
fit1
```

We get the coefficients, instead of calling the $\alpha$ intercept it is called 'mean' in this output. The rest of the output is quite similar, notice that the values of the estimated values are actually the same, this is because the internal implementation of the function is the same of the one above. The main difference is that we obtain some more information in terms of selection criteria, as a matter of fact this method returns also the corrected Aikake information and the Bayesian information criteria besides of the standard Aikake information. Remember that standard AIC works sufficiently well when we have big $n$ (large data), whereas BIC works well when we have small $n$ and AICc is actually a good compromise between the previous mentioned two criteria and it works reasonably well both for big and small $n$.
Notice also that the output format of this function can be directly used for forecasting purposes.


Moreover, this \texttt{Arima} function has an extra convinient feature which is that we can tell it if we do not want to include the $\alpha$ parameter. Meaning that if somehow we know that the mean of the model is zero then we can tell this to the function. Why is this good? Because if we have one less parameter to be estimated then we have less waste of information (meaning we are not wasting the information in our data to find out something that we already know).

```{r}
fit2 <- Arima(x, order=c(1,0,1),include.mean=FALSE)
fit2
```

As a matter of fact, estimated parameters change a little bit and the estimated errors (s.e.) are reduced, in particular for the autoregressive parameter. Moreover information criteria are smaller, meaning the this model is a better fit, and of course it is since we know data comes from an ARMA model with zero mean.

In a real situation we would choose the second model, the one with zero-mean, because it looks better but actually once we are going to analyse its residuals we will notice that they are not white noise and so we would have to go back and rethink about it.

\textbf{Model identification}


Now, we are going to learn how to decide the $p$ and $q$ parameters. The only way to do this is to treat real data because they are going to make us deal with all possible problems that can come up.

\textbf{\textit{Plot data, ACF and PACF}}


```{r}
ring<-ts(scan('Datasets/navajo.txt'),start=1263,frequency=1)
# first thing we do is to plot the data.
plot(ring)
```

In this case the plot looks like it could be flat but the mean is most certainly not zero maybe is something aroung 100. So we could be looking at a stationary ARMA model with non-zero mean.

```{r}
# then we look at the autocorrelation function
acf(ring)
```

Here we notice that this is not white noise for sure, it has this sort of sinusoidal shape consequently this is probably not just a moving average model. However we can notice that the first components are definetely positive.

```{r}
# then we look to the partial autocorrelation
pacf(ring)
```

In this way we can see if it is a pure AR model. We see again some sort of sinusoidal shape so maybe it is not, however if we consider the small values as they were actually zero we could consider there is a cutoff and that maybe we are dealing with an AR or order 1.

\textbf{\textit{Fit via ARMA(1,1) Maximum Likelihood approach}}

```{r}
ring.fit<-Arima(ring,order=c(1,0,1))
print(ring.fit)
```


\textbf{\textit{Diagnostics}}

```{r}
# Plot the residuals
tsdiag(ring.fit)

# Formal test
Box.test(residuals(ring.fit), lag=10, fitdf=2, type="Box-Pierce")
Box.test(residuals(ring.fit), lag=10, fitdf=2, type="Ljung")
```



\textbf{\textit{Fit via MA(2) Maximum Likelihood approach}}

```{r}
ring.fit2<-Arima(ring,order=c(0,0,2))
print(ring.fit2)

tsdiag(ring.fit2)

Box.test(residuals(ring.fit2), lag=10, fitdf=2, type="Box-Pierce")
Box.test(residuals(ring.fit2), lag=10, fitdf=2, type="Ljung")

AIC(ring.fit)
AIC(ring.fit2)
```


\textbf{Example}

\textbf{\textit{Remove periodic seasonality}}

```{r}
eeadj <- seasadj(stl(elecequip, s.window="periodic"))
plot(eeadj)
```


\textbf{\textit{First difference}}

```{r}
plot(diff(eeadj),main="")
acf(diff(eeadj))
pacf(diff(eeadj))
```


\textbf{\textit{Fit ARIMA(3,1,0)}}

```{r}
fit <- Arima(eeadj, order=c(3,1,0))
summary(fit)
```


\textbf{\textit{Fit ARIMA(3,1,1)}}

```{r}
fit <- Arima(eeadj, order=c(3,1,1))
summary(fit)
```


\textbf{\textit{Diagnostics }}

```{r}
acf(residuals(fit))

Box.test(residuals(fit), lag=24, fitdf=4, type="Ljung")
```


\textbf{SARIMA models}

\texttt{euretail} is a dataset on quarterly retail trade index in the Euro area. Since it's quarterly data we could expect to see a seasonality every 4 observations.

```{r}
plot(euretail, ylab="Retail index", xlab="Year")
```

We can notice (apart for the seasonality that we expected) a very strong trend and a change in the sign at a certain point. This is a particular type of series that should be analysed with something that is called a change-point model, because the plot makes us thing that something happened at a certain point (2008 crisis). As a matter of fact things behaved in a certain way up to a major event that modified the whole behaviour of this series. Another approach to this kind of series is to split the data in two parts, before and after the changing point, fit a model before then fit a model after that point and compare how different they are, if there is indeed a difference that could be attributed to the changing point.
But what we are going to do today is to see what happens if you do not use this traditional approaches and just anaalyse it as a whole.

\textbf{\textit{First difference}}

Forgetting about this changing event, the main effect we see is this sort of linear increasing trend consequently the first thing we do is to remove a difference. Then we plot thing after the first difference and we can see how things are a little bit flatter.

```{r}
d<-1
dx<-diff(euretail,lag=1,differences=d)
tsdisplay(dx)
```

Looking at ACF (we look at the lines that go outside the band) and we can notice that we see some autocorrelations outside the bands but only every four of them (this is the quarterly effect), and if we look at PACF we see something similar. If we imagine the two graphs without the lines in the middle (looking only at the ones that go outside the bands) we could state that we have a MA component of roder 1,2 or maybe 3 (looking at ACF) and an AR component (looking at PACF) always of order 1,2 or maybe 3, but they both need to be interpreted on the seasonal scale since it is the same behaviour we would expect from the AR or MA but for every time that a period has been completed.
Since we do not have an exponantial decay we can imagine we have some trend effect remaining.

\textbf{\textit{First seasonal difference (quarterly)}}

Now we are difining a seasonality

```{r}
# this is going to be the seasonal differenece, since we identified a quarterly 
# seasonality we are going to look at the seasonal difference
D<-1 
# period is 4 since we have quarterly data (four observations per year)
m<-4
# so now we are going to do the same thing as above 
# (i.e. taking the first difference) but every four observations
d4dx<-diff(dx,lag=m,differences=D)
tsdisplay(d4dx)

```

As a matter of fact this series is flatter than before, because first we removed that trend that seemed to affect the whole series and now we removed the trend that seemed to affect the seasonal observations and now we can see how in the ACF and PACF plots the autocorrelations seemed to have shrunk to fall inside the bands. Moreover now we start to see some sort of exponential decay, which tells us that the seasonal difference is a good idea.

\textbf{\textit{period m}}

```{r}
frequency(euretail) 
```


\textbf{\textit{Fit SARIMA(0,1,1)(0,1,1)$_4$}}

We are going to fit this model (we already saw why the values of $m,d,D$) and then we see from the ACF that we can have a MA component of order 1 (because of the lag 1) and then also an MA component of order 1 in the seasonal part (because of lag 4) and we try fit this.

```{r}
p<-0;d<-1;q<-1;P<-0;D<-1;Q<-1;
# fitting a seasonal arima we can apply it to the original data
# because we are going to specify d and D
fit <- Arima(euretail, order=c(p,d,q), seasonal=c(P,D,Q))
```

Notice that we may also want to check models with an AR component on order 1 and with an AR component of order 1 in the seasonal part and combinations of these things.

```{r}
# Diagnostics
tsdisplay(residuals(fit))
```

Now we have fitted the model and we are looking to the residuals, and they are sort of flat and centered at zero but we have doubt on weather they are stationary or not because we have some remaining autocorrelations or partial autocorrelations different from zero which tells us we have some correlations left in our errors, consequently this is not white noise and we need to fit another model.

\textbf{\textit{Fit SARIMA(0,1,3)(0,1,1)$_4$}}

```{r}
fit3 <- Arima(euretail, order=c(0,1,3), seasonal=c(0,1,1))
res <- residuals(fit3)
```

We can state there is not a rule on which model we need to fit and which one we do not need to fit, we need to try to fit every model that make sense and see what happens.

```{r}
# Diagnostics
tsdisplay(res)
Box.test(res, lag=16, fitdf=4, type="Ljung")
```

We can do the box test and we notice the p-value is large so the null hypothesis makes sense and this could be white noise. But can we do better? We do not know, we should explore other options.

This automatic arima does not substitute the job of thinking about what could be reasonable but if thinking about what could be reasonable we are still left with too many options this can make our work easier. Automatic arima returns the best model that can be fitted according to one of the information criterion (\texttt{ic}) we have studied, in this course we will prefer the corrected Akaike criterion. \texttt{stepwise=TRUE} means it will do stepwise selection (otherwise he will search among all possible models, i.e. it will check all possible combinations up to a certain value that we can define, and finally it will choose the one with the best criteria result) so basically it starts with one default model configuration and it starts to add or remove orders from MA and AR components until it decides that the information criteria is no longer improving. 

```{r}
## help("auto.arima")

# Search for the best model fit using stepwise procedure (BIC criterion)
# trace = TRUE will show all the models that are being compared 
auto.arima(euretail,stepwise=TRUE,trace=TRUE,ic = "bic")
```

Finally, as we said, it returns the best model according to the information criteria we choose, but we could check if there are smaller (i.e with less parameters) models with similar information criteria because sometimes it is better to sacrifice a little bit of information but remove some parameters in my model. Which one sould we choose depends on their forecasting capability.
It may also be the case that the best model chosen by the \texttt{auto.arima} has residuals not behaving as we want, so maybe we want to check some other model with a little bigger information criteria but with better residuals behaviour\footnote{It is a good idea to run this procedure both with AIC and the cAIC, if the chosen model is the same we can reason about the AIC and consider the difference among information criteria of the models significant if it is bigger than two, otherwise it means that cAIC is essentialy different from AIC and this is because we do not have enough data.}.

```{r}
# Search for the best model fit comparing "all" possible models
auto.arima(euretail, stepwise=FALSE, trace=TRUE,ic ="bic")
```


\textbf{Example}

\textbf{\textit{Stabilize the variance by transforming the data}}

```{r}
lh02 <- log(h02)
par(mfrow=c(2,1), mar=c(2,4,2,1))
plot(h02, ylab="h02 sales (million scripts)", xlab="Year")
plot(lh02, ylab="Log h02 sales", xlab="Year")
```


\textbf{\textit{Identify seasonality}}

```{r}
seasonplot(lh02)
```

\textbf{\textit{First seasonal differences (m=12)}}

```{r}
tsdisplay(diff(lh02,12), 
main="Seasonally differenced lh02 scripts", xlab="Year")
```


\textbf{\textit{Fit SARIMA(3,0,0)(2,1,0)_{12} with logaritmic transformation}}

```{r}
fit <- Arima(h02, order=c(3,0,0), seasonal=c(2,1,0), 
    lambda=0)
fit

tsdisplay(residuals(fit))

Box.test(residuals(fit), lag=36, fitdf=6, type="Ljung")
```


\textbf{\textit{Fit with automatic choice of model}}

```{r}
fit <- auto.arima(h02, lambda=0, d=0, D=1,ic="bic")
fit

tsdisplay(residuals(fit))

Box.test(residuals(fit), lag=36, fitdf=8, type="Ljung")
```



